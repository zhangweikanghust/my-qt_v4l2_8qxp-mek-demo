#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QTimer>
#include "v4l2_imx8.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();
     struct media_dev media;

private:
    Ui::Widget *ui;

    int ret;

    QImage *camera_image;

    QTimer *timer;

private slots:
    void disp_img();
};

#endif // WIDGET_H
