#ifndef V4L2_IMX8_H
#define V4L2_IMX8_H

/*
 *  Copyright 2017-2018 NXP
 */

/*
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or late.
 *
 */

/*
 * @file mx8_v4l2_cap_drm.c
 *
 * @brief MX8 Video For Linux 2 driver test application
 *
 */

/* Standard Include Files */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>
#include <poll.h>
#include <pthread.h>

#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <drm/drm.h>
#include <drm/drm_mode.h>
#include <linux/videodev2.h>


extern uint32_t log_level;
extern sigset_t sigset_v;
extern bool quitflag;

/* Helper Macro */
#define NUM_PLANES				3
#define TEST_BUFFER_NUM			3
#define NUM_SENSORS				8
#define NUM_CARDS				8
#define DEFAULT					6
#define WIDTH					1280
#define HEIGHT					800

#define INFO_LEVEL				4
#define DBG_LEVEL				5
#define ERR_LEVEL				6
#define SET_WIDTH              1280
#define SET_HEIGHT             720

#define v4l2_printf(LEVEL, fmt, args...)  \
do {                                      \
    if (LEVEL <= log_level)               \
        printf("(%s:%d): " fmt , __func__, __LINE__, ##args);   \
} while(0)

#define v4l2_info(fmt, args...)  \
        v4l2_printf(INFO_LEVEL,"\x1B[36m" fmt"\e[0m", ##args)
#define v4l2_dbg(fmt, args...)   \
        v4l2_printf(DBG_LEVEL, "\x1B[33m" fmt"\e[0m", ##args)
#define v4l2_err(fmt, args...)   \
        v4l2_printf(ERR_LEVEL, "\x1B[31m" fmt"\e[0m", ##args)


/*
 * V4L2 releated data structure definition
 */
struct plane_buffer {
    __u8 *start;
    __u32 plane_size;
    __u32 length;
    size_t offset;
};

struct testbuffer {
    struct plane_buffer planes[NUM_PLANES];
    __u32 nr_plane;
};

struct video_channel {
    int v4l_fd;
    int saved_file_fd;

    __u32 init;
    __u32 on;
    __u32 index;
    __u32 cap_fmt;
    __u32 mem_type;
    __u32 cur_buf_id;
    __u32 frame_num;

    __u32 out_width;
    __u32 out_height;

    char save_file_name[100];
    char v4l_dev_name[100];

    /* fb info */
    __s32 x_offset;
    __s32 y_offset;

    struct testbuffer buffers[TEST_BUFFER_NUM];
    __u32 nr_buffer;

    struct timeval tv1;
    struct timeval tv2;
};

struct v4l2_device {
    struct video_channel video_ch[NUM_SENSORS];
};

struct media_dev {
    struct v4l2_device *v4l2_dev;
    __u32 total_frames_cnt;
};
#ifdef __cplusplus
extern "C" {
#endif

void hello_world(void);
void global_vars_init(void);
int media_device_prepare(struct media_dev *media);
int media_device_alloc(struct media_dev *media);
void media_device_free(struct media_dev *media);
int media_device_open(struct media_dev *media);
int media_device_start(struct media_dev *media);
int media_device_stop(struct media_dev *media);
void media_device_close(struct media_dev *media);
void media_device_cleanup(struct media_dev *media);
int dqueue_buffer(int buf_id, struct video_channel *video_ch);
int queue_buffer(int buf_id, struct video_channel *video_ch);
void* signal_thread(void *arg);

#ifdef __cplusplus
}
#endif



#endif // V4L2_IMX8_H
